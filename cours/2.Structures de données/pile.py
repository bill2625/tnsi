class Pile:
    ''' classe Pile
    création d’une instance Pile avec une liste
    '''
    def __init__(self):
        "Initialisation d’une pile vide"
        self.L=[]
    def __str__(self):
        "permet l'affichage via print d'une pile"
        return str(self.L)
    def vide(self):
        "teste si la pile est vide"
        return self.L==[]
    def depiler(self):
        "dépile"
        assert not self.vide(),"Pile vide"
        return self.L.pop()
    def empiler(self,x):
        "empile"
        self.L.append(x)
    def taille(self):
        "donne le nombre d'éléments présents dans la pile"
        return len(self.L)
    def sommet(self):
        "renvoie l'élément situé en haut de la pile"
        return self.L[-1]