def pgcd(x,y):
    m = 1
    for i in range(1,x+1):
        if x%i == 0:
            if y%i == 0:
                m = i
    return m



class Rationnel:
    
    # création des instances
    def __init__(self,num,den=1):# par défaut le dénominateur vaut 1
        if den == 0:
            # on déclenche une exeption spécifique
            raise ZeroDivisionError('denominateur nul')
        else:
            #attributs d'instance
            self.num=num
            self.den=den
            self.normalise()
            
    # methode pour l'affichage
    def __str__(self):
        return str(self.num)+'/'+str(self.den)
    
    # methode pour réduire une fraction
    def normalise(self):
        g = pgcd(abs(self.num), abs(self.den))
        self.num = self.num // g
        self.den = self.den //g
        if(self.num*self.den<0):
            if self.den<0:
                self.den=-self.den
                self.num=-self.num
        else:
            if self.den <0:
                self.den=-self.den
                self.num=-self.num
                
    # méthode pour additionner deux fractions
    def __add__(self,other):
        return Rationnel(self.num*other.den+self.den*other.num,self.den*other.den)
    
    def __sub__(self,other):
        return Rationnel(self.num*other.den-self.den*other.num,self.den*other.den)
    
    def __mul__(self,other):
        return Rationnel(self.num*other.num,self.den*other.den)
    
    def __truediv__(self,other):
        if other.num == 0:
            raise ZeroDivisionError('diviseur nul')
        else:
            return Rationnel(self.num*other.den,self.den*other.num)
    

