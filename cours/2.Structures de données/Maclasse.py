class Eleve:
    '''
    Création d'une instance eleve :
    eleve = Eleve(nom(str), prenom(str), date(str), note1(programmation)(float)
    , note2(algorithmique)(float), note3(projet)(float))
    attributs de classe : matiere1,matiere2,matiere3
    attributs d'instance : nom,prenom,date,note_mat1,note_mat2,note_mat3
    méthodes :
    moyenne() qui renvoie la moyenne des 3 notes
    '''
    #attributs de classe
    matiere1 = "Programmation"
    matiere2 = "Algorithmique"
    matiere3 = "Projet"
    
    #attributs d'instance
    def __init__(self,Nom,Prenom,Date,Note1,Note2,Note3):
        self.nom = Nom
        self.prenom = Prenom
        self.date = Date
        self.note_mat1 = Note1
        self.note_mat2 = Note2
        self.note_mat3 = Note3
        
    #méthode
    def moyenne(self):
        '''
        methode qui renvoie la moyenne des 3 notes
        '''
        return ((self.note_mat1+self.note_mat2+self.note_mat3)/3)
    

def moyenne(classe):
    programmation = 0
    algo = 0
    projet = 0
    nbr_eleves = 0
    for eleve in classe:
        programmation += eleve.note_mat1
        algo += eleve.note_mat2
        projet += eleve.note_mat3
        nbr_eleves += 1
    return {'Programmation':programmation/nbr_eleves,'Algorithmique':algo/nbr_eleves,'Projet':projet/nbr_eleves}



eleve1=Eleve("Térieur","Alain","01/01/2000",12,10,15)
eleve2=Eleve("Onette","Camille","01/07/2004",7,14,11)
eleve3=Eleve("Oma","Modeste","01/1/2002",13,8,17)
classe_TA = [eleve1,eleve2,eleve3]