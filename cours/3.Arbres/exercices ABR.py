from abr import *

class Noeud:
    # Le constructeur
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right
        self.parent = None
    def __str__(self):
        return str(self.value)
    def estFeuille(self):
        if not self.left and not self.right:
            return True
        else:
            return False
    def insert(self, valeur):
        if valeur < self.value:
            if self.left is None:
                self.left = Noeud(valeur)
                self.left.parent = self
            else:
                self.left.insert(valeur)
        elif valeur > self.value:
            if self.right is None:
                self.right = Noeud(valeur)
                self.right.parent = self
            else:
                self.right.insert(valeur)
    def tri(self):
        if self:
            if self.left:
                self.left.tri()
            print(self.value,end=' ')
            if self.right:
                self.right.tri()
    def maxi(self):
        if self.right:
            return self.right.maxi()
        else:
            return self.value
    def mini(self):
        if self.left:
            return self.left.mini()
        else:
            return self.value
        
                
bst = Noeud(6)
bst.insert(8)
bst.insert(3)
bst.insert(1)
bst.insert(4)
bst.insert(9)
bst.insert(2)
bst.insert(7)
bst.insert(5)
#graphicarbre(bst)

def trier(liste):
    return arbre(liste).tri()
        
def arbre(liste):
    if liste == []:
        return Noeud()
    else:
        arbre = Noeud(liste[0])
        for element in liste[1::]:
            arbre.insert(element)
        return arbre
        
def maximum(liste):
    return arbre(liste).maxi()

def minimum(liste):
    return arbre(liste).mini()

arbre_animaux = arbre(['chat','chien','souris','araignée','crapaud','grenouille','lézard','zèbre'])