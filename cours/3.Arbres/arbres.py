from file import *

def noeud(nom, fg = None, fd = None) :
    return {'racine': nom, 'fg' : fg, 'fd': fd}
# création des noeuds
k = noeud('k')
f = noeud('f')
e = noeud('e', k, None)
b = noeud('b', e, f)
m = noeud('m')
j = noeud('j', m, None)
i = noeud('i')
d = noeud('d', i, j)
h = noeud('h')
c = noeud('c', None, h)
a = noeud('a', c, d)
racine = noeud('r', a, b)
# création de l’arbre
def construit(arbre) :
    if arbre == None :
        return []
    else:
        return [arbre['racine'],construit(arbre['fg']),construit(arbre['fd'])]
arbre1=construit(racine)
print(arbre1)
        
def hauteur(arbre):
    if arbre == []:
        return -1
    else:
        h1 = hauteur(arbre[1])+1
        h2 = hauteur(arbre[2])+1
        return max(h1,h2)


def parcoursLargeur(arbre):
    f = File()
    liste = []
    f.enfiler(arbre)
    while not f.vide():
        tmp = f.defiler()
        liste.append(tmp[0])
        if tmp[1] != []:
            f.enfiler(tmp[1])
        if tmp[2] != []:
            f.enfiler(tmp[2])
    return liste

def parcoursSuffixe(arbre):
    if arbre != []:
        parcoursSuffixe(arbre[1])
        parcoursSuffixe(arbre[2])
        print(arbre[0],end=' - ')
        
def parcoursPrefixe(arbre):
    if arbre != []:
        print(arbre[0],end=' - ')
        parcoursPrefixe(arbre[1])
        parcoursPrefixe(arbre[2])
        
        
def parcoursInfixe(arbre):
    if arbre != []:
        parcoursInfixe(arbre[1])
        print(arbre[0],end=' - ')
        parcoursInfixe(arbre[2])

